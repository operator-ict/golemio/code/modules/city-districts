# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [1.3.3] - 2024-09-12

### Changed

-   `.gitlab-ci.yml` cleanup ([devops#320](https://gitlab.com/operator-ict/golemio/devops/infrastructure/-/issues/320))

## [1.3.2] - 2024-09-05

### Added

-   asyncAPI merge yaml files ([integration-engine#258](https://gitlab.com/operator-ict/golemio/code/integration-engine/-/issues/258))

## [1.3.1] - 2024-08-16

### Added

-   add backstage metadata files
-   add .gitattributes file

## [1.3.0] - 2024-06-03

### Added

-   The `Cache-Control` header to all output gateway responses ([core#105](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/105))

## [1.2.10] - 2024-05-13

### Changed

-   Update Node.js to v20.12.2 Express to v4.19.2 ([core#102](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/102))

## [1.2.9] - 2024-01-29

### Fixed

-   IPR ArcGIS datasource ([general#530](https://gitlab.com/operator-ict/golemio/code/general/-/issues/530))

## [1.2.8] - 2023-10-09

### Added

-   Create api docs and add portman tests [#2](https://gitlab.com/operator-ict/golemio/code/modules/city-districts/-/issues/2)

## [1.2.7] - 2023-08-30

-   No changelog

## [1.2.6] - 2023-06-14

### Changed

-   Typescript version update to 5.1.3 ([core#70](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/70))

## [1.2.5] - 2023-05-31

### Changed

-   Update golemio errors ([core#62](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/62))

## [1.2.4] - 2023-04-26

### Changed

-   mongo removed ([core#66](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/66))

## [1.2.3] - 2023-03-13

### Fixed

-   Validation and change_date format

## [1.2.2] - 2023-02-27

### Changed

-   Update Node.js to v18.14.0, Express to v4.18.2 ([core#50](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/50))

## [1.2.1] - 2023-02-22

### Added

-   Postgres router ([city-districts#3](https://gitlab.com/operator-ict/golemio/code/modules/city-districts/-/issues/7))

## [1.1.0] - 2023-01-23

### Changed

-   Migrate to npm

## [1.0.6] - 2022-11-29

### Changed

-   Update gitlab-ci template

## [1.0.5] - 2022-06-07

### Changed

-   Added slug column to postgres model of city districts
-   Typescript version update from 4.4.4 to 4.6.4

## [1.0.4] - 2022-04-07

### Changed

-   Postgres model refactored into separate file.

### Added

-   New method getDiscrict for refactored model.

## [1.0.3] - 2022-03-31

### Fixed

-   Fix posgres model saving to schema common ([#3](https://gitlab.com/operator-ict/golemio/code/modules/city-districts/-/issues/3))
