import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { readFileSync } from "fs";
import { CityDistricts } from "#sch";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { CityDistrictsPostgresTransformation } from "#ie/transformations/CityDistrictsPostgresTransformation";
import { CityDistrictModel } from "#sch/models/CityDistrictModel";
import { ICityDistrictFeature } from "#sch/datasources/CityDistrictsJsonSchema";

chai.use(chaiAsPromised);

describe("CityDistrictsPostgresTransformation", () => {
    let transformation: CityDistrictsPostgresTransformation;
    let testSourceData: ICityDistrictFeature[];
    let validator: JSONSchemaValidator;

    before(() => {
        validator = new JSONSchemaValidator(CityDistricts.name + "ModelPostgresValidator", CityDistrictModel.jsonSchema);
    });

    beforeEach(async () => {
        transformation = new CityDistrictsPostgresTransformation();
        testSourceData = JSON.parse(readFileSync(__dirname + "/../../data/citydistricts-datasource.json", "utf8")).features;
    });

    it("should have name", async () => {
        expect(transformation.name).not.to.be.undefined;
        expect(transformation.name).is.equal("CityDistricts");
    });

    it("should have transform method", async () => {
        expect(transformation.transform).not.to.be.undefined;
    });

    it("should properly transform element", async () => {
        const data = await transformation.transform(testSourceData[0]);
        expect(data).to.have.property("geom");
        expect(data).to.include({
            area: null,
            change_date: null,
            change_status: null,
            create_date: null,
            district_name: "Praha-Řeporyje",
            district_short_name: "Řeporyje",
            id: 1,
            id_provider: 43,
            kod_mo: null,
            kod_so: null,
            objectid: 1,
            provider: "HMP-IPR",
            shape_area: 23964755.2832031,
            shape_length: 37597.1981885617,
            tid_tmmestckecasti_p: null,
            zip: 539635,
            district_name_slug: "praha-reporyje",
        });
    });

    it("should properly transform collection", async () => {
        const data = await transformation.transform(testSourceData);
        await expect(validator.Validate(data)).to.be.fulfilled;

        for (let i = 0, imax = data.length; i < imax; i++) {
            expect(data[i]).to.have.property("area");
            expect(data[i]).to.have.property("change_date");
            expect(data[i]).to.have.property("change_status");
            expect(data[i]).to.have.property("create_date");
            expect(data[i]).to.have.property("district_name");
            expect(data[i]).to.have.property("district_short_name");
            expect(data[i]).to.have.property("geom");
            expect(data[i]).to.have.property("id");
            expect(data[i]).to.have.property("id_provider");
            expect(data[i]).to.have.property("kod_mo");
            expect(data[i]).to.have.property("kod_so");
            expect(data[i]).to.have.property("objectid");
            expect(data[i]).to.have.property("provider");
            expect(data[i]).to.have.property("shape_area");
            expect(data[i]).to.have.property("shape_length");
            expect(data[i]).to.have.property("tid_tmmestckecasti_p");
            expect(data[i]).to.have.property("zip");
            expect(data[i]).to.have.property("district_name_slug");
        }
    });
});
