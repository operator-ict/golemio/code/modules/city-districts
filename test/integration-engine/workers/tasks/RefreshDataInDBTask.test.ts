import { RefreshDataInDBTask } from "#ie/workers/tasks";
import { config, PostgresConnector } from "@golemio/core/dist/integration-engine";
import { readFileSync } from "fs";
import { createSandbox, SinonSandbox, SinonSpy } from "sinon";
import { transformedPostgresDataFixture } from "../../../data/citydistricts-postgres-transformed";

describe("RefreshDataInDBTask", () => {
    let sandbox: SinonSandbox;
    let task: RefreshDataInDBTask;
    const testData = JSON.parse(readFileSync(__dirname + "/../../../data/citydistricts-datasource.json", "utf8"));

    beforeEach(() => {
        sandbox = createSandbox();

        sandbox.stub(PostgresConnector, "getConnection").callsFake(() => Object.assign({ define: sandbox.stub() }));
        sandbox.stub(config, "datasources").value({
            CityDistricts: "http://city-districts.cz/",
        });

        task = new RefreshDataInDBTask("test.citydistricts");
        sandbox.stub(task["dataSource"], "getAll").callsFake(() => Promise.resolve(testData));

        sandbox.stub(task["postgresTransformation"], "transform").resolves(transformedPostgresDataFixture);
        sandbox.stub(task["postgresRepository"], "bulkUpdate");
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should call the correct methods by refreshDataInDB method", async () => {
        await task["execute"]();
        sandbox.assert.calledOnce(task["dataSource"].getAll as SinonSpy);

        sandbox.assert.calledOnce(task["postgresTransformation"].transform as SinonSpy);
        sandbox.assert.calledWith(task["postgresTransformation"].transform as SinonSpy, testData);

        sandbox.assert.calledOnce(task["postgresRepository"].bulkUpdate as SinonSpy);
        sandbox.assert.calledWith(task["postgresRepository"].bulkUpdate as SinonSpy, transformedPostgresDataFixture);

        sandbox.assert.callOrder(
            task["dataSource"].getAll as SinonSpy,
            task["postgresTransformation"].transform as SinonSpy,
            task["postgresRepository"].bulkUpdate as SinonSpy
        );
    });
});
