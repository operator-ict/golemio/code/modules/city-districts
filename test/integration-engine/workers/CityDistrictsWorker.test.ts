import { createSandbox, SinonSandbox } from "sinon";
import { expect } from "chai";
import { config, PostgresConnector } from "@golemio/core/dist/integration-engine";
import { CityDistrictsWorker } from "#ie/workers/CityDistrictsWorker";

describe("CityDistrictsWorker", () => {
    let sandbox: SinonSandbox;
    let worker: CityDistrictsWorker;

    beforeEach(() => {
        sandbox = createSandbox();
        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox.stub().returns({ belongsToMany: sandbox.stub(), hasMany: sandbox.stub() }),
            })
        );
        sandbox.stub(config, "datasources").value({
            CityDistricts: "http://city-districts.cz/",
        });

        worker = new CityDistrictsWorker();
    });

    afterEach(() => {
        sandbox.restore();
    });

    describe("getQueuePrefix", () => {
        it("should have correct queue prefix set", () => {
            const result = worker["getQueuePrefix"]();
            expect(result).to.contain(".citydistricts");
        });
    });

    describe("getQueueDefinition", () => {
        it("should return correct queue definition", () => {
            const result = worker.getQueueDefinition();
            expect(result.name).to.equal("CityDistricts");
            expect(result.queues.length).to.equal(1);
        });
    });

    describe("registerTask", () => {
        it("should have two tasks registered", () => {
            expect(worker["queues"].length).to.equal(1);
            expect(worker["queues"][0].name).to.equal("refreshDataInDB");
            expect(worker["queues"][0].options.messageTtl).to.equal(15 * 24 * 60 * 60 * 1000);
        });
    });
});
