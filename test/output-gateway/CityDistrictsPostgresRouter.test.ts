import request from "supertest";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { HTTPErrorHandler, IGolemioError } from "@golemio/core/dist/shared/golemio-errors";
import express, { NextFunction, Request, Response } from "@golemio/core/dist/shared/express";
import { log } from "@golemio/core/dist/output-gateway/Logger";
import { cityDistrictsPostgresRouter } from "#og";
import { outputDataFixture } from "../data/citydistricts-output";

chai.use(chaiAsPromised);

describe("CityDistrictsPostgresRouter", () => {
    const app = express();

    before(() => {
        app.use("/citydistricts", cityDistrictsPostgresRouter);
        app.use((err: any, req: Request, res: Response, next: NextFunction) => {
            const errObject: IGolemioError = HTTPErrorHandler.handle(err, log);
            log.silly("Error caught by the router error handler.");
            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(errObject.error_status || 500).send(errObject);
        });
    });

    it("should respond with json to GET /citydistricts", (done) => {
        request(app)
            .get("/citydistricts")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200)
            .then((response) => {
                expect(response.body.features.length).to.eql(14);
                done();
            })
            .catch((err) => done(err));
    });

    it("should respond with json to GET /citydistricts with filters applied", (done) => {
        request(app)
            .get("/citydistricts?latlng=50.124935%2C14.457204&range=5000&limit=2&offset=1&updatedSince=2020-10-05T12:24:29.000Z")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200)
            .then((response) => {
                expect(response.body.features.length).to.eql(1);
                done();
            })
            .catch((err) => done(err));
    });

    it("should respond with json to GET /citydistricts/:id", (done) => {
        request(app)
            .get("/citydistricts/praha-cakovice")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200)
            .then((response) => {
                expect(response.body).to.eql(outputDataFixture);
                done();
            })
            .catch((err) => done(err));
    });

    it("should respond to GET /citydistricts with the correct Cache-Control headers", (done) => {
        request(app)
            .get("/citydistricts")
            .set("Accept", "application/json")
            .expect(200)
            .then((response) => {
                expect(response.headers["cache-control"]).to.equal("public, s-maxage=43200, stale-while-revalidate=3600");
                done();
            })
            .catch((err) => done(err));
    });

    it("should respond to GET /citydistricts/:id with the correct Cache-Control headers", (done) => {
        request(app)
            .get("/citydistricts/praha-cakovice")
            .set("Accept", "application/json")
            .expect(200)
            .then((response) => {
                expect(response.headers["cache-control"]).to.equal("public, s-maxage=43200, stale-while-revalidate=3600");
                done();
            })
            .catch((err) => done(err));
    });
});
