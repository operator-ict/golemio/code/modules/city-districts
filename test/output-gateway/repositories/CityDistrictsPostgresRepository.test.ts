import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { CityDistrictsPostgresRepository } from "#og";

chai.use(chaiAsPromised);

describe("CityDistrictsPostgresRepository", () => {
    const id: string = "praha-petrovice";
    const cityDistrictsRepository = new CityDistrictsPostgresRepository();

    it("should instantiate", () => {
        expect(cityDistrictsRepository).not.to.be.undefined;
    });

    it("should return all items", async () => {
        const result = await cityDistrictsRepository.GetAll();
        expect(result).to.be.an.instanceOf(Object);
        expect(result.features).to.be.an.instanceOf(Array);
        expect(result.features.length).to.equal(14);
    });

    it("should return single item", async () => {
        const route: any = await cityDistrictsRepository.GetOne(id);
        expect(route).not.to.be.empty;
        expect(route.properties).to.have.property("slug", id);
    });
});
