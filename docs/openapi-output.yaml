# prettier-ignore

openapi: 3.0.3
info:
    title: 🗺 Prague City Districts
    description: >-
        <p>💡 Polygons of Prague City Districts</p>
    version: 1.0.0
    contact:
        name: Golemio Prague Data Platform
        email: golemio@operatorict.cz
        url: https://golemio.cz
servers:
    - url: https://rabin.golemio.cz
      description: Test (development) server
    - url: https://api.golemio.cz
      description: Main (production) server
tags:
    - name: 🗺 Prague City Districts (v2)
      description: <img src="https://img.shields.io/badge/opendata-available-green" alt="golemioapi-opendata-badge" /> 💡 Polygons of Prague City Districts.
paths:
    /v2/citydistricts:
        get:
            tags:
                - 🗺 Prague City Districts (v2)
            summary: GET All Districts
            parameters:
                - name: latlng
                  in: query
                  description: Sorting by location (Latitude and Longitude separated by comma,
                      latitude first).
                  schema:
                      type: string
                      example: 50.124935,14.457204
                - name: range
                  in: query
                  description: Filter by distance from latlng in meters (range query). Depends
                      on the latlng parameter.
                  schema:
                      type: number
                      example: 5000
                - name: limit
                  in: query
                  description: Limits number of retrieved items.
                  schema:
                      type: number
                      example: 10
                - name: offset
                  in: query
                  description: Number of the first items that are skipped.
                  schema:
                      type: number
                      example: 0
                - name: updatedSince
                  in: query
                  description: Filters all results with older updated_at than this parameter
                  schema:
                      type: string
                      format: date-time
                      example: 2019-05-18T07:38:37.000Z
            responses:
                200:
                    description: OK
                    headers:
                        Cache-Control:
                            description: Cache control directive for caching proxies
                            schema:
                                type: string
                                example: public, s-maxage=43200, stale-while-revalidate=3600
                    content:
                        application/json; charset=utf-8:
                            schema:
                                type: object
                                properties:
                                    features:
                                        type: array
                                        items:
                                            $ref: '#/components/schemas/CityDistrictFeature'
                                    type:
                                        type: string
                                        example: FeatureCollection
                                required:
                                  - features
                                  - type
                401:
                    $ref: "#/components/responses/UnauthorizedError"
                403:
                    $ref: "#/components/responses/ForbiddenError"
    /v2/citydistricts/{id}:
        get:
            tags:
                - 🗺 Prague City Districts (v2)
            summary: GET District
            parameters:
                - name: id
                  in: path
                  description: Identifier or slug of the city district.
                  required: true
                  schema:
                      type: string
                      example: praha-1
            responses:
                200:
                    description: OK
                    headers:
                        Cache-Control:
                            description: Cache control directive for caching proxies
                            schema:
                                type: string
                                example: public, s-maxage=43200, stale-while-revalidate=3600
                    content:
                        application/json; charset=utf-8:
                            schema:
                                $ref: '#/components/schemas/CityDistrictFeature'
                401:
                    $ref: "#/components/responses/UnauthorizedError"
                403:
                    $ref: "#/components/responses/ForbiddenError"
                404:
                    description: Not found

components:
    responses:
        UnauthorizedError:
            description: API key is missing or invalid
            headers:
                x-access-token:
                    schema:
                        type: string
        ForbiddenError:
            description: Forbidden
            headers: {}
            content:
                application/json; charset=utf-8:
                    schema:
                        type: object
                        properties:
                            error_message:
                                type: string
                            error_status:
                                type: number
                        required:
                            - error_message
                            - error_status
                    examples:
                        response:
                            value:
                                error_message: Forbidden
                                error_status: 403
    schemas:
        CityDistrictFeature:
            type: object
            properties:
                geometry:
                    type: object
                    description: GeoJson geometry
                    properties:
                        coordinates:
                            oneOf:
                                -   type: array
                                    items:
                                        type: array
                                        items:
                                            type: array
                                            items:
                                                type: number
                                    example:
                                      [[
                                          [14.50823156003554, 50.10498927328214],
                                          [14.505782430390298, 50.1031042405622],
                                          [14.509701037821998, 50.1029471511537],
                                          [14.50823156003554, 50.10498927328214]
                                      ]]
                                    description: Polygon
                                -   type: array
                                    items:
                                        type: array
                                        items:
                                            type: array
                                            items:
                                                type: array
                                                items:
                                                    type: number
                                    example:
                                      [
                                          [[
                                              [14.50823156003554, 50.10498927328214],
                                              [14.505782430390298, 50.1031042405622],
                                              [14.509701037821998, 50.1029471511537],
                                              [14.50823156003554, 50.10498927328214]
                                          ]],
                                          [[
                                              [14.50823156003554, 50.10498927328214],
                                              [14.505782430390298, 50.1031042405622],
                                              [14.509701037821998, 50.1029471511537],
                                              [14.50823156003554, 50.10498927328214]
                                          ]]
                                      ]
                                    description: MultiPolygon
                        type:
                            type: string
                            enum:
                                - Polygon
                                - MultiPolygon
                properties:
                    type: object
                    properties:
                        id:
                            type: integer
                            example: 43
                        name:
                            type: string
                            example: Praha 1
                        slug:
                            type: string
                            example: praha-1
                        updated_at:
                            type: string
                            format: date-time
                            example: 2018-09-10T11:32:34.000Z
                    required:
                        - id
                        - name
                        - slug
                type:
                    type: string
                    example: Feature
