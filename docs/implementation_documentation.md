# Implementační dokumentace modulu *city-districts (Městské části Prahy)*

## Záměr

Modul slouží k uložení mapových podkladů (polygonů) všech městských částí Prahy. Tyto polygony jsou využívány primárně pro vyhledání v jaké městské části leží konkrétní gps bod.

## Vstupní data

### Data aktivně stahujeme

Data stahujeme pravidelně z IPR ArcGIS Hubu. Je to pouze jeden zdroj dat.

#### Datový zdroj *CityDistricts*

- zdroj dat
  - [IPR ArcGIS Hubu](https://services5.arcgis.com/SBTXIEUGWbqzUecw/arcgis/rest/services/MAP_CUR_MAP_MESTSKECASTI_P/FeatureServer/0/query?outFields=OBJECTID,KOD_MC,NAZEV_MC,POSKYT,ID_POSKYT,NAZEV_1&where=1%3D1&f=geojson)
  - [IE config](https://gitlab.com/operator-ict/golemio/code/integration-engine/-/blob/development/config/datasources.template.json#L10)
  - bez parametrů
- formát dat
  - HTTP GET
  - json (geojson)
  - [validační schéma](https://gitlab.com/operator-ict/golemio/code/modules/city-districts/-/blob/development/src/schema-definitions/datasources/CityDistrictsJsonSchema.ts)
  - ukázka dat
```json
{
    "type" : "FeatureCollection",
    "name" : "TMMESTSKECASTI_P",
    "features" : [
        {
            "type" : "Feature",
            "geometry" : {
                "type" : "Polygon",
                "coordinates" : [
                    [
                        [ 14.456427018000056, 50.137023240000076 ],
                        [ 14.456422205000024, 50.13706565100006 ],
                        [ 14.456333703000041, 50.13785701200004 ],
                        [ 14.456314870000028, 50.13802469500007 ],
                        [ 14.456295597000064, 50.13819642300007 ],
                        [ 14.45628073000006, 50.13832897200007 ],
                        [ 14.456265882000025, 50.13846143100005 ],
                        [ 14.456427018000056, 50.137023240000076 ]
                    ]
                ]
            },
            "properties" : {
                "OBJECTID" : 1,
                "DAT_VZNIK" : "20160422155519",
                "DAT_ZMENA" : "20160603135317",
                "PLOCHA" : 7379375.56,
                "ID" : 23,
                "KOD_MC" : 547298,
                "NAZEV_MC" : "Praha-Ďáblice",
                "KOD_MO" : 86,
                "KOD_SO" : "86",
                "TID_TMMESTSKECASTI_P" : 23,
                "POSKYT" : "HMP-IPR",
                "ID_POSKYT" : 43,
                "STAV_ZMENA" : "U",
                "NAZEV_1" : "Ďáblice",
                "Shape_Length" : 0.17478098296491398,
                "Shape_Area" : 7379375.560788
            }
        }
    ]
}
```
- frekvence stahování
  - [dev](https://gitlab.com/operator-ict/golemio/devops/infrastructure/-/blob/master/cluster_development/golemio_golemio-values.yaml#L261)
    - `0 22 7 * * *`, tzn. každý den v 7:22
  - [prod](https://gitlab.com/operator-ict/golemio/devops/infrastructure/-/blob/master/cluster_production/golemio_golemio-values.yaml#L259)
    - `0 30 2 1 * *`, tzn. první den v měsíci v 2:30
- název rabbitmq fronty
  - `dataplatform.citydistricts.refreshDataInDB`
  - bez parametrů



## Zpracování dat / transformace

Existuje pouze jeden worker. Data se nijak neobohacují ani složitě netransformují.

### Worker task _RefreshDataInDBTask_

[Odkaz](https://gitlab.com/operator-ict/golemio/code/modules/city-districts/-/blob/development/src/integration-engine/workers/tasks/RefreshDataInDBTask.ts).

- vstupní rabbitmq fronta
  - `dataplatform.citydistricts.refreshDataInDB`
  - bez parametrů
- datové zdroje
  - `CityDistricts` (viz výše)
- transformace
  - [`CityDistrictsPostgresTransformation`](https://gitlab.com/operator-ict/golemio/code/modules/city-districts/-/blob/development/src/integration-engine/transformations/CityDistrictsPostgresTransformation.ts)
  - jednoduché transformace pro uložení dat do databází
- data modely
  - [CityDistrictPostgresRepository](https://gitlab.com/operator-ict/golemio/code/modules/city-districts/-/blob/development/src/integration-engine/repositories/CityDistrictPostgresRepository.ts)
  - (viz níže)



## Uložení dat

### Obecné

- typ databáze
  - PSQL
- datábázové schéma
  - PSQL
    - jedna tabulka `citydistricts`
    - tabulka je ve schématu `common`
- retence dat
  - data se nepromazávají

### *PSQL* model

- tabulka `common.citydistricts`
- struktura tabulky
  - [model](https://gitlab.com/operator-ict/golemio/code/modules/city-districts/-/blob/development/src/schema-definitions/models/CityDistrictModel.ts)
  - migrace
    - v modulu [db-common](https://gitlab.com/operator-ict/golemio/code/modules/db-common/-/tree/development/db/migrations/common/postgresql/sqls?ref_type=heads)

## Output API

Standardní output geojson api. `CityDistrictsPostgresRouter` implementuje `GeoJsonRouter`.

### Obecné

- OpenAPI v3 dokumentace
  - [OpenAPI](./openapi.yaml)
- veřejné / neveřejné endpointy
  - veřejná open data

#### _/citydistricts_

*Postgres:*
- zdrojové tabulky
    - `citydistricts`
- dodatečná transformace: Feature collection

#### _/citydistricts/:id_

*Postgres:*
- zdrojové tabulky
    - `citydistricts`
- dodatečná transformace: Feature
