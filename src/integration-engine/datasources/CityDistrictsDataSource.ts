import {
    DataSource,
    JSONDataTypeStrategy,
    PaginatedHTTPProtocolStrategy,
} from "@golemio/core/dist/integration-engine/datasources";
import { config } from "@golemio/core/dist/integration-engine/config";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { CityDistricts } from "#sch";

export class CityDistrictsDataSourceFactory {
    public static getDataSource(): DataSource {
        return new DataSource(
            CityDistricts.name + "DataSource",
            new PaginatedHTTPProtocolStrategy({
                headers: {},
                method: "GET",
                url: config.datasources.CityDistricts,
            }),
            new JSONDataTypeStrategy({ resultsPath: "features" }),
            new JSONSchemaValidator(CityDistricts.name + "DataSourceValidator", CityDistricts.datasourceJsonSchema)
        );
    }
}
