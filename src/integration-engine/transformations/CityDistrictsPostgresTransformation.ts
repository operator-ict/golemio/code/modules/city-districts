import { CityDistricts } from "#sch";
import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine";
import slug from "slugify";
import { ICityDistricts } from "#sch/interfaces/CityDistricts";
import { ICityDistrictFeature } from "#sch/datasources/CityDistrictsJsonSchema";

export class CityDistrictsPostgresTransformation extends BaseTransformation implements ITransformation {
    public name: string;

    constructor() {
        super();
        this.name = CityDistricts.name;
    }

    protected transformElement = async (element: ICityDistrictFeature): Promise<ICityDistricts> => {
        return {
            area: null,
            change_date: null,
            change_status: null,
            create_date: null,
            district_name: element.properties.NAZEV_MC,
            district_short_name: element.properties.NAZEV_1,
            geom: {
                ...element.geometry,
                // @ts-ignore-next-line
                crs: {
                    properties: {
                        name: "EPSG:4326",
                    },
                    type: "name",
                },
            },
            id: element.id,
            id_provider: element.properties.ID_POSKYT,
            kod_mo: null,
            kod_so: null,
            objectid: element.properties.OBJECTID,
            provider: element.properties.POSKYT,
            shape_area: element.properties.SHAPE__Area,
            shape_length: element.properties.SHAPE__Length,
            tid_tmmestckecasti_p: null,
            zip: element.properties.KOD_MC,
            district_name_slug: slug(element.properties.NAZEV_MC, { lower: true, remove: /[*+~.()'"!:@]/g }),
        };
    };
}
