import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { CityDistricts } from "#sch";
import sequelize from "@golemio/core/dist/shared/sequelize";
import { CityDistrictModel } from "#sch/models/CityDistrictModel";
import { ICityDistricts } from "#sch/interfaces/CityDistricts";

export default class CityDistrictsPostgresRepository extends PostgresModel implements IModel {
    constructor() {
        super(
            CityDistricts.name + "Repository",
            {
                outputSequelizeAttributes: CityDistrictModel.attributeModel,
                pgTableName: CityDistricts.pgTableName,
                pgSchema: CityDistricts.pgSchema,
                savingType: "insertOrUpdate",
                sequelizeAdditionalSettings: { timestamps: false },
            },
            new JSONSchemaValidator(CityDistricts.name + "Validator", CityDistrictModel.jsonSchema)
        );
    }

    public getDistrict = async (longitude: number, latitude: number): Promise<string> => {
        const result = await this.findOne({
            where: sequelize.fn(
                "ST_Intersects",
                sequelize.col("geom"),
                sequelize.fn("ST_SetSRID", sequelize.fn("ST_MakePoint", longitude, latitude), 4326)
            ),
        });

        return result ? result.dataValues.district_name_slug ?? result.dataValues.district_name : null;
    };

    public bulkUpdate = async (data: ICityDistricts[]) => {
        await this.validate(data);
        await this.sequelizeModel.bulkCreate<CityDistrictModel>(data, {
            updateOnDuplicate: CityDistrictModel.updateAttributes,
        });
    };
}
