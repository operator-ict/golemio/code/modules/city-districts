import { CityDistrictsPostgresTransformation } from "#ie";
import { CityDistrictsDataSourceFactory } from "#ie/datasources";
import CityDistrictsPostgresRepository from "#ie/repositories/CityDistrictPostgresRepository";
import { AbstractEmptyTask } from "@golemio/core/dist/integration-engine";
import { DataSource } from "@golemio/core/dist/integration-engine/datasources";

export class RefreshDataInDBTask extends AbstractEmptyTask {
    public readonly queueName = "refreshDataInDB";
    public readonly queueTtl = 15 * 24 * 60 * 60 * 1000; // 15 days

    private dataSource: DataSource;
    private postgresTransformation: CityDistrictsPostgresTransformation;
    private postgresRepository: CityDistrictsPostgresRepository;

    constructor(queuePrefix: string) {
        super(queuePrefix);
        this.dataSource = CityDistrictsDataSourceFactory.getDataSource();

        this.postgresRepository = new CityDistrictsPostgresRepository();
        this.postgresTransformation = new CityDistrictsPostgresTransformation();
    }

    protected async execute(): Promise<void> {
        const data = await this.dataSource.getAll();
        const transformedPostgresData = await this.postgresTransformation.transform(data);

        await this.postgresRepository.bulkUpdate(transformedPostgresData);
    }
}
