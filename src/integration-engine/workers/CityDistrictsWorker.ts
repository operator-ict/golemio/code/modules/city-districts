import { AbstractWorker } from "@golemio/core/dist/integration-engine/workers";
import { RefreshDataInDBTask } from "#ie/workers/tasks/RefreshDataInDBTask";
import { CityDistricts } from "#sch";

export class CityDistrictsWorker extends AbstractWorker {
    protected readonly name = CityDistricts.name;

    constructor() {
        super();
        this.registerTask(new RefreshDataInDBTask(this.getQueuePrefix()));
    }
}
