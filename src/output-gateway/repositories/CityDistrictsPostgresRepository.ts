import { CityDistricts } from "#sch";
import { CityDistrictModel } from "#sch/models/CityDistrictModel";
import {
    IGeoJSONFeatureCollection,
    IGeoJsonAllFilterParameters,
    SequelizeModel,
    buildGeojsonFeatureCollection,
} from "@golemio/core/dist/output-gateway";
import { IGeoCoordinatesPolygon, buildGeojsonFeatureType } from "@golemio/core/dist/output-gateway/Geo";
import { IGeoJsonModel } from "@golemio/core/dist/output-gateway/models/interfaces/IGeoJsonModel";
import Sequelize, { Op, WhereOptions } from "@golemio/core/dist/shared/sequelize";

export interface ICityDistrictsOutput {
    id: number;
    name: string;
    slug: string;
    geom: IGeoCoordinatesPolygon;
    updated_at: string;
}
export class CityDistrictsPostgresRepository extends SequelizeModel implements IGeoJsonModel {
    private readonly DATE_FORMAT = "YYYYMMDDHH24MISS";

    constructor() {
        super(CityDistricts.name + "Repository", CityDistricts.pgTableName, CityDistrictModel.attributeModel, {
            schema: CityDistricts.pgSchema,
        });
    }

    public IsPrimaryIdNumber(): Promise<boolean> {
        return Promise.resolve(false);
    }

    public PrimaryIdentifierSelection(id: string): object {
        return { id };
    }

    public GetProperties = async () => {
        throw new Error("Not implemented");
    };

    public GetAll = async (options?: IGeoJsonAllFilterParameters): Promise<IGeoJSONFeatureCollection> => {
        const order: Sequelize.Order = [];

        const whereAttributes: WhereOptions = options
            ? {
                  [Sequelize.Op.and]: [
                      ...(options.lat && options.lng && options.range
                          ? [
                                Sequelize.fn(
                                    "ST_DWithin",
                                    Sequelize.col("geom"),
                                    Sequelize.cast(
                                        Sequelize.fn("ST_SetSRID", Sequelize.fn("ST_MakePoint", options.lng, options.lat), 4326),
                                        "geography"
                                    ),
                                    options.range
                                ),
                            ]
                          : []),
                      ...(options.updatedSince
                          ? [
                                Sequelize.where(
                                    Sequelize.fn("to_timestamp", Sequelize.literal("change_date"), this.DATE_FORMAT),
                                    Op.gte,
                                    options.updatedSince
                                ),
                            ]
                          : []),
                  ],
              }
            : {};

        if (options?.lat && options.lng) {
            order.push(
                Sequelize.fn(
                    "ST_Distance",
                    Sequelize.col("geom"),
                    Sequelize.cast(
                        Sequelize.fn("ST_SetSRID", Sequelize.fn("ST_MakePoint", options.lng, options.lat), 4326),
                        "geography"
                    )
                )
            );
        }

        const result = (await this.sequelizeModel.findAll({
            attributes: [
                "id",
                ["district_name", "name"],
                ["district_name_slug", "slug"],
                "geom",
                [Sequelize.fn("to_timestamp", Sequelize.literal("change_date"), this.DATE_FORMAT), "updated_at"],
            ],
            raw: true,
            where: whereAttributes,
            limit: options?.limit,
            offset: Number.isInteger(options?.offset) ? options?.offset : undefined,
            order,
        })) as ICityDistrictsOutput[];

        return buildGeojsonFeatureCollection(
            result.map((record: ICityDistrictsOutput) => {
                return this.formatOutput(record);
            })
        );
    };

    public GetOne = async (id: string) => {
        const result = (await this.sequelizeModel.findOne({
            attributes: [
                "id",
                ["district_name", "name"],
                ["district_name_slug", "slug"],
                "geom",
                [
                    Sequelize.literal(
                        `(to_timestamp(change_date, '${this.DATE_FORMAT}')::timestamp at time zone 'Europe/Prague')`
                    ),
                    "updated_at",
                ],
            ],
            where: { district_name_slug: id },
            raw: true,
        })) as ICityDistrictsOutput;

        return result ? this.formatOutput(result) : undefined;
    };

    private formatOutput = (record: ICityDistrictsOutput) => {
        return buildGeojsonFeatureType("geom", record);
    };
}
