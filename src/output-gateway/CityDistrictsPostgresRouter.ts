import { Router } from "@golemio/core/dist/shared/express";
import { GeoJsonRouter } from "@golemio/core/dist/output-gateway";
import { CityDistrictsPostgresRepository } from "#og/repositories";

export class CityDistrictsPostgresRouter extends GeoJsonRouter {
    constructor() {
        super(new CityDistrictsPostgresRepository());
        this.initRoutes({ maxAge: 12 * 60 * 60, staleWhileRevalidate: 60 * 60 });
    }
}

const cityDistrictsPostgresRouter: Router = new CityDistrictsPostgresRouter().router;

export { cityDistrictsPostgresRouter };
