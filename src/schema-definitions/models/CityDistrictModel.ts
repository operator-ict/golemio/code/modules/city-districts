import { Polygon } from "@golemio/core/dist/shared/geojson";
import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import Sequelize, { Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { SharedSchemaProvider } from "@golemio/core/dist/schema-definitions";
import { ICityDistricts } from "#sch/interfaces/CityDistricts";

export class CityDistrictModel extends Model<ICityDistricts> implements ICityDistricts {
    declare id: number;
    declare area: number;
    declare change_date: string;
    declare change_status: string;
    declare create_date: string;
    declare district_name: string;
    declare district_short_name: string;
    declare geom: Polygon;
    declare id_provider: number;
    declare kod_mo: number;
    declare kod_so: string;
    declare objectid: number;
    declare provider: string;
    declare shape_area: number;
    declare shape_length: number;
    declare tid_tmmestckecasti_p: number;
    declare zip: number;
    declare district_name_slug: string;

    public static attributeModel: ModelAttributes<CityDistrictModel, ICityDistricts> = {
        area: Sequelize.DOUBLE(25),
        change_date: Sequelize.STRING,
        change_status: Sequelize.STRING,
        create_date: Sequelize.STRING,
        district_name: Sequelize.STRING,
        district_short_name: Sequelize.STRING,
        geom: Sequelize.GEOMETRY,
        id: {
            primaryKey: true,
            type: Sequelize.INTEGER,
        },
        id_provider: Sequelize.INTEGER,
        kod_mo: Sequelize.INTEGER,
        kod_so: Sequelize.STRING,
        objectid: Sequelize.INTEGER,
        provider: Sequelize.STRING,
        shape_area: Sequelize.DOUBLE(25),
        shape_length: Sequelize.DOUBLE(25),
        tid_tmmestckecasti_p: Sequelize.INTEGER,
        zip: Sequelize.INTEGER,
        district_name_slug: Sequelize.STRING,
    };

    public static updateAttributes = Object.keys(CityDistrictModel.attributeModel).filter((att) => att !== "id") as Array<
        keyof ICityDistricts
    >;

    public static jsonSchema: JSONSchemaType<ICityDistricts[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                area: { oneOf: [{ type: "number" }, { type: "null" }] },
                change_date: { oneOf: [{ type: "string" }, { type: "null" }] },
                change_status: { oneOf: [{ type: "string" }, { type: "null" }] },
                create_date: { oneOf: [{ type: "string" }, { type: "null" }] },
                district_name: { type: "string" },
                district_short_name: { type: "string" },
                geom: { $ref: "#/definitions/geometry" },
                id: { type: "integer" },
                id_provider: { type: "integer" },
                kod_mo: { oneOf: [{ type: "integer" }, { type: "null" }] },
                kod_so: { oneOf: [{ type: "string" }, { type: "null" }] },
                objectid: { type: "integer" },
                provider: { type: "string" },
                shape_area: { type: "number" },
                shape_length: { type: "number" },
                tid_tmmestckecasti_p: { oneOf: [{ type: "integer" }, { type: "null" }] },
                zip: { type: "integer" },
                district_name_slug: { type: "string" },
            },
            required: ["id", "geom", "district_name", "district_name_slug"],
        },
        definitions: {
            // @ts-expect-error
            geometry: SharedSchemaProvider.Geometry,
        },
    };
}
