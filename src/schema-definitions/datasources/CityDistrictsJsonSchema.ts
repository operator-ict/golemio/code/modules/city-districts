import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { SharedSchemaProvider } from "@golemio/core/dist/schema-definitions";
import { Polygon } from "@golemio/core/dist/shared/geojson";

export interface ICityDistrictFeature {
    type: string;
    id: number;
    geometry: Polygon;
    properties: ICityDistrictPropertiesInput;
}

export interface ICityDistrictPropertiesInput {
    ID_POSKYT: number;
    KOD_MC: number;
    NAZEV_1: string;
    NAZEV_MC: string;
    OBJECTID: number;
    POSKYT: string;
    SHAPE__Area: number;
    SHAPE__Length: number;
}

export const cityDistrictsDatasourceJsonSchema: JSONSchemaType<ICityDistrictFeature[]> = {
    type: "array",
    items: {
        type: "object",
        required: ["type", "id", "geometry", "properties"],
        additionalProperties: false,
        properties: {
            type: { type: "string" },
            id: { type: "number" },
            geometry: { $ref: "#/definitions/geometry" },
            properties: {
                type: "object",
                required: ["KOD_MC", "NAZEV_MC"],
                additionalProperties: false,
                properties: {
                    ID_POSKYT: { type: "integer" },
                    KOD_MC: { type: "integer" },
                    NAZEV_1: { type: "string" },
                    NAZEV_MC: { type: "string" },
                    OBJECTID: { type: "integer" },
                    POSKYT: { type: "string" },
                    SHAPE__Area: { type: "number" },
                    SHAPE__Length: { type: "number" },
                },
            },
        },
    },
    definitions: {
        // @ts-expect-error
        geometry: SharedSchemaProvider.Geometry,
    },
};
