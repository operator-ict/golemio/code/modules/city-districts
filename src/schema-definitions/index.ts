import { cityDistrictsDatasourceJsonSchema } from "#sch/datasources/CityDistrictsJsonSchema";

const forExport: any = {
    name: "CityDistricts",
    pgSchema: "common",
    pgTableName: "citydistricts",
    mongoCollectionName: "citydistricts",
    datasourceJsonSchema: cityDistrictsDatasourceJsonSchema,
};

export { forExport as CityDistricts };
