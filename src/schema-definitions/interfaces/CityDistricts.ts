import { Polygon } from "@golemio/core/dist/shared/geojson";

export interface ICityDistricts {
    id: number;
    area: number | null;
    change_date: string | null;
    change_status: string | null;
    create_date: string | null;
    district_name: string;
    district_short_name: string;
    geom: Polygon;
    id_provider: number;
    kod_mo: number | null;
    kod_so: string | null;
    objectid: number;
    provider: string;
    shape_area: number;
    shape_length: number;
    tid_tmmestckecasti_p: number | null;
    zip: number;
    district_name_slug: string;
}
